package report;

import core.Application;
import core.ApplicationListener;
import core.DTNHost;

//add AMSApplication
import applications.AMSApplication;
/**
 * Reporter for the <code>AMSApplication</code>. It counts all 
 * types of messages used by AMS and calculaues the success 
 * probabilities. 
 * 
 * @author Ali Shoker
 */
public class AMSAppReporter extends Report implements ApplicationListener {
	
	private int anonymSent=0, anonymReceived=0,anonymArrived2Dest=0,forwardAnonym=0,pauseAnonym=0;
	private int ackSent=0, ackReceived=0,ackNotArrived2Dest=0;
	private int askSent=0, askReceived=0,askNotArrived2Dest=0;
	private int keySent=0, keyReceived=0,keyNotArrived2Dest=0;
	
	public void gotEvent(String event, Object params, Application app,
			DTNHost host) {
		// Check that the event is sent by correct application type
		if (!(app instanceof AMSApplication)) return;
		
		// Increment the counters based on the event type
		if (event.equalsIgnoreCase("sendAnonym"))  anonymSent++;
		if (event.equalsIgnoreCase("inAnonym")) anonymReceived++;
		if (event.equalsIgnoreCase("forwardAnonym")) forwardAnonym++;
		if (event.equalsIgnoreCase("pauseAnonym")) pauseAnonym++;
		if (event.equalsIgnoreCase("Anonym_A2D")) anonymArrived2Dest++;
		
		if (event.equalsIgnoreCase("sendAsk")) 	askSent++;
		if (event.equalsIgnoreCase("inAsk")) askReceived++;
		if (event.equalsIgnoreCase("Ask_!A2D")) askNotArrived2Dest++;
		
		if (event.equalsIgnoreCase("sendKey")) 	keySent++;
		if (event.equalsIgnoreCase("inKey")) keyReceived++;
		if (event.equalsIgnoreCase("Key_!A2D")) keyNotArrived2Dest++;
		
		if (event.equalsIgnoreCase("sendAck")) 	ackSent++;
		if (event.equalsIgnoreCase("inAck")) ackReceived++;
		if (event.equalsIgnoreCase("Ack_!A2D")) ackNotArrived2Dest++;
		
	}

	
	@Override
	public void done() {
		write("Anonym stats for scenario " + getScenarioName() + 
				"\nsim_time: " + format(getSimTime()));
		double anonymProb = 0; // anonym probability
		double askProb = 0; // ask probability
		double successProb = 0;	// success probability
		
		if (this.anonymSent > 0) {
			anonymProb = (1.0 * this.anonymArrived2Dest) / this.anonymSent;
		}
		if (this.ackSent > 0) {
			askProb = (1.0 * (this.ackReceived-this.ackNotArrived2Dest)) / this.ackSent;
		}
		if (this.anonymSent > 0) {
			successProb = (1.0 * this.ackReceived) / this.anonymSent;
		}
	
		
		String statsText = "ANONYM sent: " + this.anonymSent + 
			"\nANONYM received: " + this.anonymReceived + 
			"\nANONYM arrived to dest: " + this.anonymArrived2Dest+
			"\nANONYM paused: " + this.pauseAnonym+
			"\nANONYM forwarded: " + this.forwardAnonym+
			"\nASK sent: " + this.askSent +
			"\nASK received: " + this.askReceived +
			"\nKEY sent: " + this.keySent +
			"\nKEY received: " + this.keyReceived +
			"\nACK sent: " + this.ackSent +
			"\nACK received: " + this.ackReceived +
			"\nACK arrived/not to dest: " +(this.ackReceived-this.ackNotArrived2Dest)+
			"/"+this.ackNotArrived2Dest+
			"\nANONYM delivery prob: " + format(anonymProb) +
			"\nACK delivery prob: " + format(askProb) + 
			"\nANONYM/ACK success prob: " + format(successProb)
			;
		
		write(statsText);
		super.done();
	}
}
