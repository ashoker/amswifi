# This configuration file is a simple application that is is used to 
# simulate AMS protocol. The file also uses a report class called 
# "AMSAppReporter". 
#
# Define new application
AMSApp.type = AMSApplication
AMSApp.interval = 500
AMSApp.destinationRange = 0,125
AMSApp.pingSize = 10k
AMSApp.pongSize = 1k
AMSApp.passive = false

# Set Ping app for all nodes
Group.nrofApplications = 1
Group.application1 = AMSApp

# Add report for Ping app
Report.nrofReports = 2
Report.report2 = AMSAppReporter

#Group.router = EpidemicRouter
#Group.router = DirectDeliveryRouter
Group.router = MaxPropRouter
#Group.router = FirstContactRouter
#Group.router = ProphetRouter
#Group.router = SprayAndWaitRouter
#SprayAndWaitRouter.nrofCopies = 10
#SprayAndWaitRouter.binaryMode = true
