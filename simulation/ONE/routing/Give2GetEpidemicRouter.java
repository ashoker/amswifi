package routing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import routing.Give2GetEpidemicRouter.MetaData;

import com.sun.xml.internal.rngom.digested.DDataPattern.Param;
import com.sun.xml.internal.ws.api.addressing.WSEndpointReference.Metadata;

import core.ApplicationListener;
import core.Connection;
import core.ConnectionListener;
import core.DTNHost;
import core.Message;
import core.Settings;
import core.SimClock;
import core.Tuple;

/**
 * Give2Get epidemic router for rational behaviors published  in the IEEE
 * International Conference on Distributed Computing Systems (ICDCS'10) and
 * as described in this link:
 * "wwwusers.di.uniroma1.it/~mei/sitoweb/Publications_files/mei-ICDCS10.pdf"
 * NOTE: In this implementation, was don't consider all the one-way messaging
 * steps of the protocol, but we enlarge the message size to simulate the 
 * delay of message exchange. Our purpose is to check if Give2Get detects
 * misbehavior across different traces (considering encounters and
 * re-encounters).
 * 
 *  @author Ali Shoker
 */
public class Give2GetEpidemicRouter extends ActiveRouter {


	public final int DELTA=24*3600;//time in seconds

	/**
	 * 
	 * @author Ali Shoker
	 * A class to store needed information about messages
	 *
	 */

	public class MetaData{

		int d1;//delta1 of Give2Get
		int d2;//delta2 of Give2Get
		int nbOfSends;//nb of sends required to send a message only twice/host


		/**
		 * Constructor. Creates a new MetaData instance and sets the
		 * attributes as defined in the passed parameters.
		 * @param d1 delta1 of Give2Get
		 * @param d2 delta2 of Give2Get
		 * @param nbOfSends number times this message has been send by this host
		 * 
		 */

		public MetaData(int d1, int d2, int nbOfSends){
			this.d1=d1;
			this.d2=d2;
			this.nbOfSends=nbOfSends;
		}


		/**
		 * Copy constructor. Creates a new MetaData instance and sets the
		 * same values as defined in the passed object.
		 * @param original the MetaData object to take a copy from
		 * 
		 */

		public MetaData(MetaData original){
			this.d1=original.d1;
			this.d2=original.d2;
			this.nbOfSends=original.nbOfSends;
		}
	}

	/**
	 * Map of messages meta information used to forward/delete a message
	 */
	Map<String, MetaData> messagesMetaData;
	/**
	 * Map of hosts address() to list of message ids in the messagesMetaData map.
	 */
	Map<Integer, List<String>> hostToMessagesMap;
	/**
	 * List of misbehaving nodes
	 */
	List<Integer> blacklist;
	/**
	 * Number of misbehaving nodes
	 */
	public int nbOfMisbehavior;

	/**
	 * Number of encounters between delta1 and delta2
	 */
	public int nbOfDeltaEncounters;

	/**
	 * A modulo number that represents the misbehaving nodes, a misbehaving
	 *  node has the value (hostAddress % nbOfDroppersModulo==0) true
	 */
	static final int nbOfDroppersModulo=3;


	/**
	 * Constructor. Creates a new message router based on the settings in
	 * the given Settings object.
	 * @param s The settings object
	 */
	public Give2GetEpidemicRouter(Settings s) {
		super(s);

		messagesMetaData= new HashMap<String,MetaData>();
		hostToMessagesMap= new HashMap<Integer,List<String>>();
		blacklist=new ArrayList<Integer>();
	}

	/**
	 * Copy constructor.
	 * @param r The router prototype where setting values are copied from
	 */
	protected Give2GetEpidemicRouter(Give2GetEpidemicRouter r) {
		super(r);

		messagesMetaData= new HashMap<String,MetaData>();
		hostToMessagesMap= new HashMap<Integer,List<String>>();
		blacklist=new ArrayList<Integer>();
		nbOfMisbehavior=r.nbOfMisbehavior;
		nbOfDeltaEncounters=r.nbOfDeltaEncounters;

		Iterable<Entry<String,MetaData>> rMetaData=r.messagesMetaData.entrySet();
		for (Entry<String, MetaData> entry : rMetaData) {
			this.messagesMetaData.put(entry.getKey(),
					new MetaData(entry.getValue()));	
		}

		Iterable<Entry<Integer, List<String>>> rHosts=r.hostToMessagesMap.entrySet();
		for (Entry<Integer, List<String>> entry : rHosts) {
			this.hostToMessagesMap.put(entry.getKey(),
					new ArrayList<String>(entry.getValue()));	
		}

		int size=r.blacklist.size();
		for (int i = 0; i < size; i++) 
			this.blacklist.add(r.blacklist.get(i));

	}


	@Override
	public void changedConnection(Connection con) {


		int rhost=-1;
		if(con.isUp()){
			rhost=con.getOtherNode(getHost()).getAddress();

			List<String> remoteHostMessages=hostToMessagesMap.get(rhost);

			if(remoteHostMessages!=null)

			{
				MetaData meta=null;
				int now=(int) SimClock.getTime();
				for (int i = 0; i < remoteHostMessages.size(); i++) {
					//System.out.println(messagesMetaData.get(remoteHostMessages.get(i)).d1);
					meta=messagesMetaData.get(remoteHostMessages.get(i));

					if(meta!=null && meta.d1 < now && now < meta.d2){// a misbehavior is be detected
						if(!blacklist.contains(rhost) && rhost % nbOfDroppersModulo==0){
							blacklist.add(rhost);
							//here delete all metadata and host
							for (int j = 0; j < remoteHostMessages.size(); j++)
								messagesMetaData.remove(remoteHostMessages.get(j));

							hostToMessagesMap.remove(rhost);	
							nbOfMisbehavior++;
							System.out.println(
									"localhost:"+getHost()+
									", blacklisted host"+ rhost +
									", nb of misbehaviors:"+nbOfMisbehavior+
									", blacklist size:"+blacklist.size());

						}
						nbOfDeltaEncounters++;
						if(nbOfDeltaEncounters %10==0)
							System.out.println(
									"localhost:"+getHost()+
									", remote host"+ rhost +
									", nb of delta encounters:"+nbOfDeltaEncounters);

						break;
					}

				}
			}

		}
	};

	@Override
	protected int checkReceiving(Message m) {
		int recvCheck = super.checkReceiving(m); 
		//System.out.println("Localhost:"+ getHost()+", from host:"+m.getFrom()+
		//	", destination host:"+m.getTo());
		if (recvCheck == RCV_OK) {
			/* don't accept a message that has already traversed this node */
			if (m.getHops().contains(getHost()) || (m.getHopCount()>0 && blacklist.contains(
					m.getHops().get(m.getHopCount()-1)))) 
			{recvCheck = DENIED_OLD;
			}

		}

		return recvCheck;
	}

	@Override
	public void update() {

		super.update();

		DTNHost localhost=getHost();
		Message[] messages = getMessageCollection().toArray(new Message[0]);
		if((localhost.getAddress() % nbOfDroppersModulo)==0){ // droppers
			for (int i=0; i<messages.length; i++) 
				//drop message if localhost is not source or destination
				if (!messages[i].getTo().equals(localhost) && 
						!messages[i].getFrom().equals(localhost)) 
					deleteMessage(messages[i].getId(), true);
		}

		List<String> ids;
		for (Iterator<List<String>> iterator = hostToMessagesMap.values().iterator();
				iterator.hasNext();) {
			ids =  iterator.next();
			for (int i = 0; i < ids.size(); i++) 
				if(messagesMetaData.get(ids.get(i)).d2<SimClock.getTime())
					messagesMetaData.remove(ids.remove(i));		
		}


		if (isTransferring() || !canStartTransfer()) {
			return; 
		}

		if (exchangeDeliverableMessages() != null) {
			return; 
		}


		Connection conxn=tryAllMessagesToAllConnections();
		if(conxn!=null && blacklist.contains(conxn.getOtherNode(getHost()).getAddress()))
			conxn.abortTransfer();

	}

	@Override
	protected void transferDone(Connection con) {

		// add message metaData to this host
		String mId=con.getMessage().getId();
		int otherAddr=con.getOtherNode(getHost()).getAddress();

		if(!messagesMetaData.containsKey(mId) ){
			int delta=(int) (SimClock.getTime()+DELTA);
			messagesMetaData.put(mId, new MetaData(delta, 2*delta,	0));

			if(!hostToMessagesMap.containsKey(otherAddr))
				hostToMessagesMap.put(otherAddr,new ArrayList<String>());
			
			hostToMessagesMap.get(otherAddr).add(mId);
		}

		messagesMetaData.get(mId).nbOfSends++;
		/* delete the senders copy if message has been sent twice */
		if(messagesMetaData.get(mId).nbOfSends>1)
			this.deleteMessage(mId, false);
	}

	@Override
	public Give2GetEpidemicRouter replicate() {
		return new Give2GetEpidemicRouter(this);
	}

}
