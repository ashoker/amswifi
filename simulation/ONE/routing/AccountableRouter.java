package routing;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import com.sun.org.apache.bcel.internal.generic.ALOAD;

import core.Connection;
import core.DTNHost;
import core.Message;
import core.Settings;

/**
 * Accountable message forwarding router which tolerates rational 
 * behaviours and prevents them from dropping others messages. This however
 * comes at the cost of higher communication traffic and latency since
 * each message has to be sent in four communication steps (the message
 * with encrypted destination and then the key).
 *
 * @author Ali Shoker
 **/
public class AccountableRouter extends ActiveRouter {

	/** identifier for the number of copies to forward setting ({@value})*/ 
	public static final String NR_OF_COPIES = "nrofCopies";
	/** identifier for the number of punishment copies to forward setting ({@value})*/ 
	public static final String NR_OF_PUNISH_COPIES = "nrofPunishCopies";
	/** identifier for the percentage of rational nodes setting ({@value}).
	 *  An integer value between 2 and 100 included; otherwise, no rational nodes
	 *  are assumed.
	 **/ 
	public static final String PERCENT_OF_RATIONAL = "precentOfRational";
	/** Accountable router's settings name space ({@value})*/ 
	public static final String ACCOUNTABLE_ROUTER_NS = "AccountableRouter";

	protected int nbOfCopies;
	protected int nbOfPunishCopies;
	protected int percentOfRational;
	protected final int nbOfHosts;
	protected final boolean allowRational;
	protected Set<Integer> rationalNodesSet;
	/** A map that contains the number of copies sent per msgId **/
	protected Map<String, Integer> messageSends;
	
	/**
	 * Constructor. Creates a new message router based on the settings in
	 * the given Settings object.
	 * @param s The settings object
	 */
	public AccountableRouter(Settings s) {
		super(s);
		Settings accSettings = new Settings(ACCOUNTABLE_ROUTER_NS);
		Settings groupSettings = new Settings("Group");

		nbOfCopies= accSettings.getInt(NR_OF_COPIES);
		nbOfPunishCopies= accSettings.getInt(NR_OF_PUNISH_COPIES);
		percentOfRational= accSettings.getInt(PERCENT_OF_RATIONAL);
		nbOfHosts= groupSettings.getInt("nrofHosts");

		messageSends=new HashMap<String,Integer>();

		rationalNodesSet=null;

		if(percentOfRational>100 || percentOfRational <0){
			allowRational=false;
			return;
		} 
		else{

			allowRational=true;
			int nbOfRationalNodes=percentOfRational * nbOfHosts / 100;
			rationalNodesSet= new HashSet<Integer>();

			int randomHost=(int) Math.random()* nbOfHosts;//random host
			for (int i = 0; i < nbOfRationalNodes; i++) {
				//find a new random host address
				while(rationalNodesSet.contains(randomHost))
					randomHost=(int) Math.random()* nbOfHosts;

				rationalNodesSet.add(randomHost);
			}
		}

	}

	/**
	 * Copy constructor.
	 * @param r The router prototype where setting values are copied from
	 */
	protected AccountableRouter(AccountableRouter r) {
		super(r);
		nbOfCopies=r.nbOfCopies;
		nbOfHosts=r.nbOfHosts;
		nbOfPunishCopies=r.nbOfPunishCopies;
		percentOfRational=r.percentOfRational;
		allowRational=r.allowRational;

		Entry<String, Integer> entry;
		for (Iterator<Entry<String,Integer>> iterator = r.messageSends.entrySet().iterator();
				iterator.hasNext();) {
			entry=iterator.next();
			messageSends.put(entry.getKey(),entry.getValue());
		}

		if(!allowRational)
			rationalNodesSet=null;
		else{
			rationalNodesSet=new HashSet<Integer>();
			for (Iterator<Integer> iterator = r.rationalNodesSet.iterator();
					iterator.hasNext();) 
				rationalNodesSet.add(iterator.next());
		}
	}

	@Override
	protected int checkReceiving(Message m) {
		int recvCheck = super.checkReceiving(m); 

		if (recvCheck == RCV_OK) {
			/* don't accept a message that has already traversed this node */
			if (m.getHops().contains(getHost()) ||
					(allowRational && rationalNodesSet.contains(m.getProperty("Sender")))) 
				recvCheck = DENIED_OLD;

		}

		return recvCheck;
	}

	@Override
	public void update() {
		super.update();
		if (isTransferring() || !canStartTransfer()) {
			return; 
		}

		if (exchangeDeliverableMessages() != null) {
			return; 
		}

		if(allowRational){
			DTNHost localhost=getHost();
			Message[] messages = getMessageCollection().toArray(new Message[0]);
			for (int i=0; i<messages.length; i++) 
				//drop message if localhost is rational and it is not source or destination
				if (rationalNodesSet.contains(localhost) &&
						!messages[i].getTo().equals(localhost) && 
						!messages[i].getFrom().equals(localhost)) 
					deleteMessage(messages[i].getId(), true);

		}

		tryAllMessagesToAllConnections();
	}

	@Override
	protected void transferDone(Connection con) {
		/* don't leave a copy for the sender */
		String mId=con.getMessage().getId();
		
		Integer oldCount=messageSends.get(mId);
		int count=1;
		if(oldCount!=null)
			count=oldCount+1;

		// here we don't consider rational nodes that should send
		// a punishment of nbOfPunishCopies. In case this is needed
		// (e.g., to test performance of when punishments are sent)
		// then add condition similar to this:
		// (|| (rationalNodesSet.contains(getHost()) && count>=nbOfPunishCopies)) 
		if(count>=nbOfCopies) {
			this.deleteMessage(con.getMessage().getId(), false);
			messageSends.remove(mId);
		} 
		else
			messageSends.put(mId, new Integer(count));
	}

	@Override
	public AccountableRouter replicate() {
		return new AccountableRouter(this);
	}

}
