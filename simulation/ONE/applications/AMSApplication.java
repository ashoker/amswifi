package applications;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import com.sun.tools.doclets.internal.toolkit.util.Group;

import core.Application;
import core.CBRConnection;
import core.Connection;
import core.DTNHost;
import core.Message;
import core.NetworkInterface;
import core.Settings;
import core.SimClock;
import core.SimScenario;
import core.World;

//add the following report
import report.PingAppReporter;

/** 
 * This is a simple application for ONE that simulates the
 * Accountable messaging system (AMS). The code mimics the 
 * ping/pong protocol. The main logic of the 
 * protocols is not implemented here, but only the general 
 * dataflow and message forwarding that is needed to conduct 
 * the simulation.
 *
 * @author Ali Shoker
 *
 **/
public class AMSApplication extends Application {
	/** Run in passive mode - don't generate pings but respond */
	public static final String PING_PASSIVE = "passive";
	/** Ping generation interval */
	public static final String PING_INTERVAL = "interval";
	/** Ping interval offset - avoids synchronization of ping sending */
	public static final String PING_OFFSET = "offset";
	/** Destination address range - inclusive lower, exclusive upper */
	public static final String PING_DEST_RANGE = "destinationRange";
	/** Seed for the app's random number generator */
	public static final String PING_SEED = "seed";
	/** Size of the ping message */
	public static final String PING_PING_SIZE = "pingSize";
	/** Size of the pong message */
	public static final String PING_PONG_SIZE = "pongSize";
	
	Map<Integer, Message> savedAnonyms=new HashMap<Integer, Message>();

	/** Application ID */
	public static final String APP_ID = "fr.cnrs.liris.drim.AMSApplication";
	public enum AMS_M_TYPE {ANONYM,ASK,KEY,ACK};


	// Private vars
	private double	lastPing = 0;
	private double	interval = 500;
	private boolean passive = false;
	private int		seed = 0;
	private int		destMin=0;
	private int		destMax=1;
	private int		pingSize=1;
	private int		pongSize=1;
	private Random	rng;

	/** 
	 * Creates a new AMSApplication with the given settings.
	 * 
	 * @param s	Settings to use for initializing the application.
	 */
	public AMSApplication(Settings s) {
		if (s.contains(PING_PASSIVE)){
			this.passive = s.getBoolean(PING_PASSIVE);
		}
		if (s.contains(PING_INTERVAL)){
			this.interval = s.getDouble(PING_INTERVAL);
		}
		if (s.contains(PING_OFFSET)){
			this.lastPing = s.getDouble(PING_OFFSET);
		}
		if (s.contains(PING_SEED)){
			this.seed = s.getInt(PING_SEED);
		}
		if (s.contains(PING_PING_SIZE)) {
			this.pingSize = s.getInt(PING_PING_SIZE);
		}
		if (s.contains(PING_PONG_SIZE)) {
			this.pongSize = s.getInt(PING_PONG_SIZE);
		}
		if (s.contains(PING_DEST_RANGE)){
			int[] destination = s.getCsvInts(PING_DEST_RANGE,2);
			this.destMin = destination[0];
			this.destMax = destination[1];
		}

		rng = new Random(this.seed);
		super.setAppID(APP_ID);
		
	}

	/** 
	 * Copy-constructor
	 * 
	 * @param a
	 */
	public AMSApplication(AMSApplication a) {
		super(a);
		this.lastPing = a.getLastPing();
		this.interval = a.getInterval();
		this.passive = a.isPassive();
		this.destMax = a.getDestMax();
		this.destMin = a.getDestMin();
		this.seed = a.getSeed();
		this.pongSize = a.getPongSize();
		this.pingSize = a.getPingSize();
		this.rng = new Random(this.seed);
		
	}

	/** 
	 * Handles an incoming message. If the message is a ping message replies
	 * with a pong message. Generates events for ping and pong messages.
	 * 
	 * @param msg	message received by the router
	 * @param host	host to which the application instance is attached
	 */
	@Override
	public Message handle(Message msg, DTNHost host) {
		AMS_M_TYPE type = (AMS_M_TYPE)msg.getProperty("msgType");
		String mId="";
		Message m;

		if (type==null) return msg; // Not a ping/pong message

		// Respond with pong if we're the recipient
		DTNHost sender=msg.getHops().get(msg.getHopCount()-1);
		//System.out.println("sender:"+sender.getAddress());
		//System.out.println("source:"+msg.getFrom().getAddress());
		//System.out.println("source:"+(msg.getFrom().getAddress()==sender.getAddress()));

		switch (type) {
		case ANONYM:
			mId="ASK"+SimClock.getIntTime() + "-" + 
					host.getAddress();
			m= new Message(host, sender, mId, getPongSize());
			m.addProperty("msgType", AMS_M_TYPE.ASK);
			//add anonym message Id to maintain continuous chain
			m.addProperty("msgId", msg.getId());
			super.sendEventToListeners("inAnonym", null, host);
			super.sendEventToListeners("sendAsk", null, host);
			m.setAppID(APP_ID);
			host.createNewMessage(m);
			super.sendEventToListeners("pauseAnonym", null, host);
			
			Message newM= msg.replicate();
				savedAnonyms.put(newM.getId().hashCode(),newM);
			return null;
		case ACK:
			super.sendEventToListeners("inAck", null, host);
			if(host!=msg.getTo())
				super.sendEventToListeners("Ack_!A2D", null, host);
			return null;

		case ASK:
			super.sendEventToListeners("inAsk", null, host);

			if(msg.getTo()!=host)
				super.sendEventToListeners("Ask_!A2D", null, host);

			else if(msg.getTo()==host){
				mId="KEY"+SimClock.getIntTime() + "-" + 
						host.getAddress();
				m= new Message(host, sender, mId, getPongSize());
				m.addProperty("msgType", AMS_M_TYPE.KEY);
				m.addProperty("msgId", msg.getProperty("msgId"));
				super.sendEventToListeners("sendKey", null, host);
				m.setAppID(APP_ID);
				host.createNewMessage(m);

			}
			return null;

		case KEY:
			super.sendEventToListeners("inKey", null, host);
			if(msg.getTo()!=host)
			{
				super.sendEventToListeners("Key_!A2D", null, host);
				return null;
			}
			else if (msg.getTo()==host)
			{
				mId="ACK"+SimClock.getIntTime() + "-" + 
						host.getAddress();
				m= new Message(host, sender, mId, getPongSize());
				m.addProperty("msgType", AMS_M_TYPE.ACK);
				host.createNewMessage(m);
				super.sendEventToListeners("sendAck", null, host);				

				for (Entry<Integer, Message> entry : savedAnonyms.entrySet()) {
				//	System.out.println("equal Id:"+entry.getKey().equals(
					//		msg.getProperty("msgId").hashCode()));
					if(entry.getKey().equals(msg.getProperty("msgId").hashCode()))
						if(entry.getValue().getTo()==host){
							super.sendEventToListeners("Anonym_A2D", null, host);
							savedAnonyms.remove(entry.getKey());
							return null;
						}
						else{
							super.sendEventToListeners("forwardAnonym", null, host);
							return savedAnonyms.remove(entry.getKey());
						}

				}

			}
			break;
		default:
			break;
		}

		return null;
	}

	/** 
	 * Draws a random host from the destination range
	 * 
	 * @return host
	 */
	private DTNHost randomHost() {
		int destaddr = 0;
		if (destMax == destMin) {
			destaddr = destMin;
		}
		destaddr = destMin + rng.nextInt(destMax - destMin);
		World w = SimScenario.getInstance().getWorld();
		return w.getNodeByAddress(destaddr);
	}

	@Override
	public Application replicate() {
		return new AMSApplication(this);
	}

	/** 
	 * Sends a ping packet if this is an active application instance.
	 * 
	 * @param host to which the application instance is attached
	 */
	@Override
	public void update(DTNHost host) {
		if (this.passive) return;
		double curTime = SimClock.getTime();
		if (curTime - this.lastPing >= this.interval) {
			// Time to send a new ping
			Message m = new Message(host, randomHost(), "ANONYM" +
					SimClock.getIntTime() + "-" + host.getAddress(),
					getPingSize());
			m.addProperty("msgType", AMS_M_TYPE.ANONYM);
			m.setAppID(APP_ID);
			host.createNewMessage(m);

			// Call listeners
			super.sendEventToListeners("sendAnonym", null, host);

			this.lastPing = curTime;
		}
	}

	/**
	 * @return the lastPing
	 */
	public double getLastPing() {
		return lastPing;
	}

	/**
	 * @param lastPing the lastPing to set
	 */
	public void setLastPing(double lastPing) {
		this.lastPing = lastPing;
	}

	/**
	 * @return the interval
	 */
	public double getInterval() {
		return interval;
	}

	/**
	 * @param interval the interval to set
	 */
	public void setInterval(double interval) {
		this.interval = interval;
	}

	/**
	 * @return the passive
	 */
	public boolean isPassive() {
		return passive;
	}

	/**
	 * @param passive the passive to set
	 */
	public void setPassive(boolean passive) {
		this.passive = passive;
	}

	/**
	 * @return the destMin
	 */
	public int getDestMin() {
		return destMin;
	}

	/**
	 * @param destMin the destMin to set
	 */
	public void setDestMin(int destMin) {
		this.destMin = destMin;
	}

	/**
	 * @return the destMax
	 */
	public int getDestMax() {
		return destMax;
	}

	/**
	 * @param destMax the destMax to set
	 */
	public void setDestMax(int destMax) {
		this.destMax = destMax;
	}

	/**
	 * @return the seed
	 */
	public int getSeed() {
		return seed;
	}

	/**
	 * @param seed the seed to set
	 */
	public void setSeed(int seed) {
		this.seed = seed;
	}

	/**
	 * @return the pongSize
	 */
	public int getPongSize() {
		return pongSize;
	}

	/**
	 * @param pongSize the pongSize to set
	 */
	public void setPongSize(int pongSize) {
		this.pongSize = pongSize;
	}

	/**
	 * @return the pingSize
	 */
	public int getPingSize() {
		return pingSize;
	}

	/**
	 * @param pingSize the pingSize to set
	 */
	public void setPingSize(int pingSize) {
		this.pingSize = pingSize;
	}

}
